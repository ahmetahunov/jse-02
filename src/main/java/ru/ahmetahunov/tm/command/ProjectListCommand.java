package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

import java.util.*;

class ProjectListCommand implements Command {
    @Override
    public void execute() {
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : CommandExecutor.projects.getProjects()) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
            int j = 1;
            List<Task> tasks = CommandExecutor.tasks.getTasksByProjectId(project.getId());
            if (tasks.size() != 0)
                System.out.println("Task:");
            for (Task task : tasks) {
                System.out.println(String.format("  %d) %s", j++, task.getName()));
            }
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return "project-list: Show all projects with tasks.";
    }
}