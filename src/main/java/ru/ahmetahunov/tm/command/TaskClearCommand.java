package ru.ahmetahunov.tm.command;

class TaskClearCommand implements Command {
    @Override
    public void execute() {
        CommandExecutor.tasks.clear();
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println();
    }

    @Override
    public String toString() {
        return "task-clear: Remove all tasks.";
    }
}