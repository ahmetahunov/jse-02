package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

import java.io.IOException;
import java.util.List;

public class SelectProjectCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[SELECT PROJECT]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = CommandExecutor.projects.getProject(name);
        if (project == null) {
            System.out.println("Selected project not exists.");
            return;
        }
        System.out.println(project.getName());
        List<Task> tasks = CommandExecutor.tasks.getTasksByProjectId(project.getId());
        if (tasks.size() != 0)
            System.out.println("Task:");
        int i = 1;
        for (Task task : tasks) {
            System.out.println(String.format("  %d. %s", i++, task.getName()));
        }
    }

    @Override
    public String toString() {
        return "select-project: Show selected project.";
    }
}
