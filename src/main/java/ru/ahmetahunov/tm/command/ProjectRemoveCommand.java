package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class ProjectRemoveCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        for (Project project : new ArrayList<>(CommandExecutor.projects.getProjects())) {
            if (project.getName().equals(name)) {
                CommandExecutor.projects.remove(project);
                List<Task> tasks = CommandExecutor.tasks.getTasksByProjectId(project.getId());
                for (Task task : tasks) {
                    CommandExecutor.tasks.remove(task);
                }
                System.out.println("[OK]");
                System.out.println();
                return;
            }
        }
        System.out.println("Selected project not exists.");
        System.out.println();
    }

    @Override
    public String toString() {
        return "project-remove: Remove selected project.";
    }
}