package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

import java.io.IOException;

class TaskCreateCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.print("Enter project name or press enter to skip: ");
        Project project = getProject();
        System.out.print("Please enter task name: ");
        String name = ConsoleHelper.readMessage().trim();
        Task task = CommandExecutor.tasks.getTask(name, (project == null) ? 0 : project.getId());
        if (task != null) {
            System.out.println(name + " already exists.");
            if (!isReplacedTask(task))
                return;
        }
        task = new Task(name);
        CommandExecutor.tasks.add(task);
        if (project != null) {
            task.setProjectId(project.getId());
        }
        System.out.println("[OK]");
        System.out.println();
    }

    private Project getProject() throws IOException {
        String name = ConsoleHelper.readMessage().trim();
        if (!("".equals(name))) {
            Project project = CommandExecutor.projects.getProject(name);
            if (project != null)
                return project;
            String answer = null;
            System.out.println(name + " is not available.");
            do {
                System.out.println("Do you want use another project?<y/n>");
                answer = ConsoleHelper.readMessage();
                if ("y".equals(answer))
                    return getProject();
            } while (!("n".equals(answer)));
        }
        return null;
    }

    private boolean isReplacedTask(Task task) throws IOException {
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                System.out.println();
                return false;
            }
        }while (!("y".equals(answer)));
        CommandExecutor.tasks.remove(task);
        return true;
    }

    @Override
    public String toString() {
        return "task-create: Create new task.";
    }
}