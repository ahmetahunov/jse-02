package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;
import java.io.IOException;
import java.util.List;

class ProjectCreateCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = CommandExecutor.projects.getProject(name);
        if (project != null) {
            System.out.println(name + " already exists.");
            if (!isReplacedProject(project))
                return;
        }
        project = new Project(name);
        CommandExecutor.projects.add(project);
        System.out.println("[OK]");
        System.out.println();
    }

    private boolean isReplacedProject(Project project) throws IOException {
        String answer = null;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                System.out.println();
                return false;
            }
        } while (!("y".equals(answer)));
        CommandExecutor.projects.remove(project);
        List<Task> tasks = CommandExecutor.tasks.getTasksByProjectId(project.getId());
        for (Task task : tasks) {
            CommandExecutor.tasks.remove(task);
        }
        return true;
    }

    @Override
    public String toString() {
        return "project-create: Create new project.";
    }
}