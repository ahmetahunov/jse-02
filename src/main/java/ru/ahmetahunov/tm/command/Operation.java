package ru.ahmetahunov.tm.command;

public enum Operation {
    HELP,
    EXIT,
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_REMOVE,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_REMOVE,
    SELECT_PROJECT,
    UNKNOWN
}