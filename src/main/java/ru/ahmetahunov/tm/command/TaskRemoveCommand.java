package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

import java.io.IOException;


class TaskRemoveCommand implements Command {
    @Override
    public void execute() throws IOException {
        System.out.println("[TASK REMOVE]");
        System.out.print("Enter project name or press enter to skip:");
        String name = ConsoleHelper.readMessage().trim();
        Project project = null;
        int projectId = 0;
        if (!"".equals(name)) {
            project = CommandExecutor.projects.getProject(name);
            if (project == null) {
                System.out.println("Selected project not exists");
                System.out.println("[OPERATION CANCELLED]");
                return;
            }
            projectId = project.getId();
        }
        System.out.print("Enter task name: ");
        name = ConsoleHelper.readMessage().trim();
        Task task = CommandExecutor.tasks.getTask(name, projectId);
        if (task != null) {
            CommandExecutor.tasks.remove(task);
            System.out.println("[OK]");
            return;
        }
        System.out.println("Selected task not exists.");
    }

    @Override
    public String toString() {
        return "task-remove: Remove selected task.";
    }
}