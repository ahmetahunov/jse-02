package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.Task;

class TaskListCommand implements Command {
    @Override
    public void execute() {
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : CommandExecutor.tasks.getTasks()) {
            System.out.println(String.format("%d. %s", i++, task.getName()));
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return "task-list: Show all tasks.";
    }
}