package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.model.Project;
import ru.ahmetahunov.tm.model.Task;

class ProjectClearCommand implements Command {
    @Override
    public void execute() {
        for (Project project : CommandExecutor.projects.getProjects()) {
            for (Task task : CommandExecutor.tasks.getTasksByProjectId(project.getId()))
                CommandExecutor.tasks.remove(task);
        }
        CommandExecutor.projects.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
        System.out.println();
    }

    public String toString() {
        return "project-clear: Remove all projects.";
    }
}