package ru.ahmetahunov.tm;

import ru.ahmetahunov.tm.command.CommandExecutor;
import ru.ahmetahunov.tm.listener.TaskListener;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        TaskListener listener = new TaskListener(new CommandExecutor());
        System.out.println( "*** WELCOME TO TASK MANAGER ***" );
        listener.listen();
    }
}
