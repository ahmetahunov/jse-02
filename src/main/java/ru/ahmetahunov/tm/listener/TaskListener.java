package ru.ahmetahunov.tm.listener;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.enumeration.Operation;
import ru.ahmetahunov.tm.command.CommandExecutor;
import java.io.IOException;

public class TaskListener {
    private final CommandExecutor executor;

    public TaskListener(CommandExecutor executor) {
        this.executor = executor;
    }

    public void listen() throws IOException {
        while (true) {
            Operation operation = getOperation(ConsoleHelper.readMessage());
            executor.execute(operation);
            if (operation == Operation.EXIT) {
                ConsoleHelper.close();
                break;
            }
        }
    }

    private Operation getOperation(String command) {
        command = command.trim().toLowerCase();
        if ("help".equals(command))
            return Operation.HELP;
        else if ("exit".equals(command))
            return Operation.EXIT;
        else if ("project-clear".equals(command))
            return Operation.PROJECT_CLEAR;
        else if ("project-create".equals(command))
            return Operation.PROJECT_CREATE;
        else if ("project-list".equals(command))
            return Operation.PROJECT_LIST;
        else if ("project-remove".equals(command))
            return Operation.PROJECT_REMOVE;
        else if ("task-clear".equals(command))
            return Operation.TASK_CLEAR;
        else if ("task-create".equals(command))
            return Operation.TASK_CREATE;
        else if ("task-list".equals(command))
            return Operation.TASK_LIST;
        else if ("task-remove".equals(command))
            return Operation.TASK_REMOVE;
        else if ("select-project".equals(command))
            return Operation.SELECT_PROJECT;
        return Operation.UNKNOWN;
    }
}
