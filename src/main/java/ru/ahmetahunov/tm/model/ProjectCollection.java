package ru.ahmetahunov.tm.model;

import java.util.ArrayList;
import java.util.List;

public class ProjectCollection {
    private final List<Project> projects;

    public ProjectCollection() {
        this.projects = new ArrayList<>();
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Project getProject(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    public Project getProjectById(int id) {
        for (Project project : projects) {
            if (project.getId() == id)
                return project;
        }
        return null;
    }

    public void add(Project project) {
        projects.add(project);
    }

    public void remove(Project project) {
        projects.remove(project);
    }

    public void clear() {
        projects.clear();
    }
}
