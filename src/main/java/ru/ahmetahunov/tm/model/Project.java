package ru.ahmetahunov.tm.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Project {
    private static int counter = 1;
    private String name;
    private int id;

    public Project(String name) {
        this.name = name;
        this.id = counter++;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(name, project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
