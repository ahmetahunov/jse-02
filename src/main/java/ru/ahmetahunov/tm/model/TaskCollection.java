package ru.ahmetahunov.tm.model;

import java.util.*;

public class TaskCollection {
    private final Map<Integer, Task> tasks = new LinkedHashMap<>();

    public Collection<Task> getTasks() {
        return tasks.values();
    }

    public Task getTask(String name, int projectId) {
        for (Task task : getTasks()) {
            if (task.getName().equals(name) && task.getProjectId() == projectId)
                return task;
        }
        return null;
    }

    public List<Task> getTasksByProjectId(int projectId) {
        List<Task> tasks = new ArrayList<>();
        for (Task task : getTasks()) {
            if (task.getProjectId() == projectId) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    public Map<Integer, Task> getTasksMap() { return tasks; }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(Task task) {
        tasks.remove(task.getId());
    }

    public void clear() {
        tasks.clear();
    }
}
