# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.com/ahmetahunov/jse-02.git
cd jse-02
mvn clean install
```

## run app
```bash
java -jar target/jse-02-1.0.1.jar
```